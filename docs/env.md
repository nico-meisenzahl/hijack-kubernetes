# Spin up the environment

This guide is based on Azure and Azure Kubernetes Service but should also work with any other Public Cloud offering.

## Kubernetes Cluster

Create Resource Group:

```bash
az group create --name demo-rg --location westeurope
```

Create AKS:

```bash
az aks create -n demo-aks \
 -g demo-rg \
 -k 1.19.7 \
 -l westeurope \
 --enable-managed-identity \
 -m 250 \
 -c 1 \
 -s Standard_B2m
```

Configure kubectl:

```bash
az aks get-credentials -n demo-aks -g demo-rg
```

Deploy the sample app:

```bash
kubectl apply -f https://gitlab.com/nico-meisenzahl/hijack-kubernetes/-/raw/main/assets/demo.yaml
```

## Attacker host

You will also need a host (virtual machine) with a public IP and open port (to connect the reverse shell too). I would recommend spinning up a virtual machine in Azure (be aware to configure the NSG to allow access).
